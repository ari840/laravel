<!DOCTYPE HTML>
<HTML>
	<head>
		<title>@yield('title') page</title>
		<meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">    
	    <meta http-equiv="content-type" content="text/html; charset=utf-8">
	    <meta name="author" content="">
	    <meta name="generator" content="">

	    <link rel="shortcut icon" href="content/img/favicon.png">
	    <!-- Bootstrap CSS -->
	    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">    
	    <link rel="stylesheet" href="css/jasny-bootstrap.min.css" type="text/css">    
	    <link rel="stylesheet" href="css/jasny-bootstrap.min.css" type="text/css">
	    <!-- Material CSS -->
	    <link rel="stylesheet" href="css/material-kit.css" type="text/css">
	    <!-- Font Awesome CSS -->
	    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
	        <!-- Line Icons CSS -->
	    <link rel="stylesheet" href="content/frontend/fonts/line-icons/line-icons.html" type="text/css">
	        <!-- Line Icons CSS -->
	    <link rel="stylesheet" href="content/frontend/fonts/line-icons/line-icons.html" type="text/css">
	    <!-- Animate CSS -->
	    <link rel="stylesheet" href="css/animate.css" type="text/css">
	    <!-- Owl Carousel -->
	    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
	    <link rel="stylesheet" href="css/owl.theme.css" type="text/css">    
	    <!-- Responsive CSS Styles -->
	    <link rel="stylesheet" href="css/responsive.css" type="text/css">
	    <!-- Slicknav js -->
	    <link rel="stylesheet" href="css/slicknav.css" type="text/css">
	        <!-- Bootstrap Select -->
	    <link rel="stylesheet" href="css/bootstrap-select.min.css">
	      <!-- Main Styles -->
	    <link rel="stylesheet" href="css/main.css" type="text/css">

	     <link rel="stylesheet" href="css/extra.css" type="text/css">

	    <script type="text/javascript" src="js/jquery-min.js"></script>      
	    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	    <script type="text/javascript" src="js/material.min.js"></script>
	    <script type="text/javascript" src="js/material-kit.js"></script>
	    <script type="text/javascript" src="js/jquery.parallax.js"></script>
	    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
	    <script type="text/javascript" src="js/wow.js"></script>
	    <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
	    <script type="text/javascript" src="js/waypoints.min.js"></script>
	    <script type="text/javascript" src="js/jasny-bootstrap.min.js"></script>
	    <script type="text/javascript" src="js/bootstrap-select.min.js"></script>
	    <script type="text/javascript" src="js/main.js"></script>
	</head>
	<body>
		<!-- Header Section Start -->
	 	<div class="header">    
	      <nav class="navbar navbar-default main-navigation" role="navigation">
	        <div class="container">
	          <div class="navbar-header">
	            <!-- Stat Toggle Nav Link For Mobiles -->
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	              <span class="sr-only">Toggle navigation</span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	              <span class="icon-bar"></span>
	            </button>
	            <!-- End Toggle Nav Link For Mobiles -->
	            <a class="navbar-brand logo" href="home.html"><img src="img/logo.png" alt="Course Admission System"></a>
	          </div>
	          <!-- brand and toggle menu for mobile End -->

	          <!-- Navbar Start -->
	          <div class="collapse navbar-collapse" id="navbar">
	            <ul class="nav navbar-nav navbar-right">
	            <li><a href="#">Find Course</a></li>
	            <li><a href="#">About Us</a></li>
	            <li><a href="#"><i class="lnr lnr-enter"></i> Login</a></li>
	              
	              <li class="postadd">
	                <a class="btn btn-info btn-post" href="#"><span class="fa fa-user"></span> Register</a>
	              </li>
	            </ul>
	          </div>
	          <!-- Navbar End -->
	        </div>
	      </nav>
	     
	    </div>    <!-- Header Section End -->


			@yield('content')
			

			<!-- Footer Section Start -->
	  	<footer>
	    	<!-- Footer Area Start -->
	    	<section class="footer-Content">
	    		<div class="container">
	    			<div class="row">
	    				<div class="col-md-8 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0">
	              <div class="widget">
	                <h3 class="block-title">About us</h3>
	                <div class="textwidget">
	                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed, auctor ut purus.</p>
	                </div>
	              </div>
	            </div>
	    				<div class="col-md-4 col-sm-6 col-xs-12 wow fadeIn" data-wow-delay="0.5">
	    					<div class="widget">
	    						<h3 class="block-title">Useful Links</h3>
	  							<ul class="menu">
	                  <li><a href="#">Home</a></li>
	                  <li><a href="#">Categories</a></li>
	                  <li><a href="#">FAQ</a></li>
	                  <li><a href="#">Left Sidebar</a></li>
	                  <li><a href="#">Pricing Plans</a></li>
	                  <li><a href="#">About</a></li>
	                  <li><a href="#">Contact</a></li>
	                  <li><a href="#">Full Width Page</a></li>
	                  <li><a href="#">Terms of Use</a></li>
	                  <li><a href="#">Privacy Policy</a></li>
	                </ul>
	    					</div>
	    				</div>
	    			</div>
	    		</div>
	    	</section>
	    	<!-- Footer area End -->
	    	
	    	<!-- Copyright Start  -->
	    	<div id="copyright">
	    		<div class="container">
	    			<div class="row">
	    				<div class="col-md-12">
	              <div class="site-info pull-left">
	                <p>All copyrights reserved @ 2017</a></p>
	              </div>    					
	              <div class="bottom-social-icons social-icon pull-right">  
	                <a class="facebook" target="_blank" href="#"><i class="fa fa-facebook"></i></a> 
	                <a class="twitter" target="_blank" href="#"><i class="fa fa-twitter"></i></a>
	                <a class="youtube" target="_blank" href="#"><i class="fa fa-youtube"></i></a>
	                <a class="linkedin" target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
	              </div>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    	<!-- Copyright End -->

	    </footer>    <!-- Footer Section End -->  	
	</body>


	@stack('scripts')

</HTML>