@extends('layout.layouts')

@section('title','Welcome')


@section('content')
    <div class="banner_bg"> 
  <form class="form-signin ">
    <div class="container ">
      <div class="row ">
      <div class="col-md-4  hidden-xs"></div><div class="col-md-4  hidden-xs"></div>
        <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="panel panel-default borderRound">
        <div class="borderRound">
        
         
        
            <div class="panel-body">   
            
                  <div class="form-group">
                  <button class="btn btn-social btn-primary btn-facebook btn-block" >
              Login with Facbook
            </button>
            <h3 class="text-center">
                or</h3>
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span>
                      </span>
                      <input type="text" class="form-control" placeholder="Username/email">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                      <input type="password" class="form-control" placeholder="Password">
                    </div>
                  </div>
                  
                             
                    <button class="btn btn-lg btn-primary btn-block" type="submit">
              Login
            </button>
                    
            
            </div>
             
         </div>
        </div>
      </div>
      </div>
    </div>
  </form> 
</div>
<div class="wrapper">

  <!-- Categories Homepage Section Start -->
  <section id="categories-homepage">
    <div class="container">
      <div class="row">
         <section id="categories-homepage">
        <div class="container">
      <div class="row">
          <div class="col-md-9">
          <div class="col-md-12">
            <h3 class="section-title text-center">Browse by Discipline</h3>
          </div>  
          
           <div class="col-md-4 col-sm-4 col-xs-6">   
            <div class="wow fadeInUpQuick animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
              <a href="#">
              <div class="img-btn-padding">
                <img  class="img-responsive img-rounded" src="img/University_Admission.png" /> 
              </div>
              </a>
            </div>
          </div>  
           <div class="col-md-4 col-sm-4 col-xs-6"> 
            <div class="wow fadeInUpQuick animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
              <a href="#">
              <div class="img-btn-padding">
                <img  class="img-responsive img-rounded" src="img/BCS_Preparation.png" /> 
              </div>
              </a>
            </div>
          </div>  
           <div class="col-md-4 col-sm-4 col-xs-6"> 
            <div class="wow fadeInUpQuick animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
               <div class="img-btn-padding">
               <a href="#">
                <img  class="img-responsive img-rounded" src="img/Engineering_Admission.png" /> 
              </a>
              </div>
            </div>
          </div>  
           
          <div class="col-md-4 col-sm-4 col-xs-6">  
            <div class="wow fadeInUpQuick animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
              <div class="img-btn-padding">
              <a href="#">
                <img  class="img-responsive img-rounded" src="img/Other_Govt_Job_Preparation.png" /> 
              </a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">  
            <div class="wow fadeInUpQuick animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
              <div class="img-btn-padding">
              <a href="#">
                <img  class="img-responsive img-rounded" src="img/Medical_Admission.png" /> 
              </a>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-6">  
            <div class="wow fadeInUpQuick animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
              <div class="img-btn-padding">
              <a href="#">
                <img  class="img-responsive img-rounded" src="img/Bank_Job_Preparation.png" /> 
              </a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div>
              <h3 class="section-title text-center">Notice Board</h3>
            </div>
          <div class="test_div borderRound">
             <ul>
               @foreach($Notices as $notice)
                    <li><a href="{{action('NoticeController@notice',['id'=>$notice->id])}}" target="Blank">{{ $notice->Title }}</a></li>
               @endforeach
             </ul>
          </div>
        </div>
      </div>  
             
       </section> 
      <section id="categories-homepage">
    <div class="container">
    </div>
    </section> 
   </div>   
    </div>
    </div>
  </section>

  <!-- Categories Homepage Section End -->

  <!-- Featured Listings Start -->
  <section class="featured-lis" >
    <div class="container">
      <div class="row">
        <div class="col-md-12 wow fadeIn" data-wow-delay="0.5s">
          <h3 class="section-title">Featured Listings</h3>
          <div id="new-products" class="owl-carousel">
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img1.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div>    
                <a href="#" class="item-name">Lorem ipsum dolor sit</a>  
                <span class="price">$150</span>  
              </div>
            </div>
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img2.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div> 
                <a href="#" class="item-name">Sed diam nonummy</a>  
                <span class="price">$67</span> 
              </div>
            </div>
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img3.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div>
                <a href="#" class="item-name">Feugiat nulla facilisis</a>  
                <span class="price">$300</span>  
              </div>
            </div>
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img4.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div> 
                <a href="#" class="item-name">Lorem ipsum dolor sit</a>  
                <span class="price">$149</span> 
              </div>
            </div>
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img5.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div>
                <a href="#" class="item-name">Sed diam nonummy</a>  
                <span class="price">$90</span> 
              </div>
            </div>
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img6.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div>                     
                <a href="#" class="item-name">Praesent luptatum zzril</a>  
                <span class="price">$169</span> 
              </div>
            </div>
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img7.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div>  
                <a href="#" class="item-name">Lorem ipsum dolor sit</a>  
                <span class="price">$79</span> 
              </div>
            </div>
            <div class="item">
              <div class="product-item">
                <div class="carousel-thumb">
                  <img src="img/product/img8.jpg" alt=""> 
                  <div class="overlay">
                    <a href="#"><i class="fa fa-link"></i></a>
                  </div> 
                </div>
                <a href="#" class="item-name">Sed diam nonummy</a>  
                <span class="price">$149</span>   
              </div>
            </div>
          </div>
        </div> 
      </div>
    </div>
  </section>  
  <!-- Featured Listings End -->

  <!-- Start Services Section -->
  <div class="features">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="0.3s">
            <div class="features-icon">
              <i class="fa fa-book">
              </i>
            </div>
            <div class="features-content">
              <h4>
                Find University
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="0.6s">
            <div class="features-icon">
              <i class="fa fa-paper-plane"></i>
            </div>
            <div class="features-content">
              <h4>
                Find Course
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="0.9s">
            <div class="features-icon">
              <i class="fa fa-map"></i>
            </div>
            <div class="features-content">
              <h4>
                Get Mentors
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div> 
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="1.2s">
            <div class="features-icon">
              <i class="fa fa-cogs"></i>
            </div>
            <div class="features-content">
              <h4>
                Find Course
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>           
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="1.5s">
            <div class="features-icon">
             <i class="fa fa-hourglass"></i>
            </div>
            <div class="features-content">
              <h4>
                Get Mentors
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="1.8s">
            <div class="features-icon">
              <i class="fa fa-hashtag"></i>
            </div>
            <div class="features-content">
              <h4>
                Find University
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="2.1s">
            <div class="features-icon">
              <i class="fa fa-newspaper-o"></i>
            </div>
            <div class="features-content">
              <h4>
                Find University
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="2.4s">
            <div class="features-icon">
              <i class="fa fa-leaf"></i>
            </div>
            <div class="features-content">
              <h4>
                Find University
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="features-box wow fadeInDownQuick" data-wow-delay="2.7s">
            <div class="features-icon">
              <i class="fa fa-google"></i>
            </div>
            <div class="features-content">
              <h4>
                Find University
              </h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo aut magni perferendis repellat rerum assumenda facere. 
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Services Section -->
 
  <!-- Location Section Start -->
  <section class="location">
    <div class="container">
      <div class="row text-center">
          <div class="error-page">
            <h2><a href="#">All Universities In Your Fingerprint</a></h2>
            <a href="#" class="btn btn-danger btn-lg">Search  Now</a>
        </div>
      </div>
    </div>
  </section>
  <!-- Location Section End -->

</div>
@endsection


@push('scripts')
@endpush