@extends('layout.layouts')

@section('title','Welcome')


@section('content')
	<div class="container">
		@foreach($notice as $n)
		<div class="text-center" style="height: 500px;">
			<h2>{{ $n->Title }}</h2>
			<p>{{ $n->Description }}</p>
		</div>
		@endforeach
	</div>
@endsection


@push('scripts')
@endpush